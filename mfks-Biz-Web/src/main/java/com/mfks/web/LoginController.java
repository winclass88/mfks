package com.mfks.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.mfks.bean.WxUserInfo;
import com.mfks.model.TMember;
import com.mfks.service.MemberService;
import com.mfks.service.SysParamService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import top.ibase4j.core.Constants;
import top.ibase4j.core.Constants.MsgChkType;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.exception.BusinessException;
import top.ibase4j.core.exception.LoginException;
import top.ibase4j.core.support.Assert;
import top.ibase4j.core.support.http.SessionUser;
import top.ibase4j.core.support.security.coder.HmacCoder;
import top.ibase4j.core.util.CacheUtil;
import top.ibase4j.core.util.DataUtil;
import top.ibase4j.core.util.HttpUtil;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.PropertiesUtil;
import top.ibase4j.core.util.SecurityUtil;
import top.ibase4j.core.util.WebUtil;

/**
 * 用户登录
 *
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:11:21
 */
@RestController
@RequestMapping("/app/")
@Api(value = "APP登录注册接口", description = "APP-登录注册接口")
public class LoginController extends AppBaseController<TMember, MemberService> {
    private SysParamService paramService;

    @PostMapping("reginit")
    @ApiOperation(value = "注册", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "使用手机号+验证码进行注册或登录\n"
            + "注册接口需要以下五个参数：\n" + "1. UUID(header): 客户端生成的唯一ID，用作用户令牌，服务端用之识别用户，验证权限，每接口必传\n"
            + "2. sign: 请求参数的RSA签名(通过/app/secret.api获取RSA私钥)，用于防止请求参数被第三方拦截篡改，每接口必传。签名算法查看密钥接口\n"
            + "3. account: 用户名，目前只支持手机号\n" + "4. password: 密码\n" + "5. authCode: 手机短信验证码(通过/app/msg.api发送短信验证码)\n"
            + "注意：所有接口都需要传UUID和sign参数，UUID用作令牌，sign用作签名", response = TMember.class)
    public Object register(@RequestHeader("UUID") @ApiParam(value = "客户端生成的唯一ID", required = true) String uuid,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password,
        @RequestParam @ApiParam(value = "手机验证码", required = true) String authCode) throws Exception {
        String authCodeOnServer = (String)CacheUtil.getCache().get(MsgChkType.REGISTER + account);
        if (!authCode.equals(authCodeOnServer)) {
            logger.warn(account + "=" + authCode + "-" + authCodeOnServer);
            throw new IllegalArgumentException("手机验证码错误");
        }

        Map<String, Object> params = InstanceUtil.newHashMap("loginKey", account);
        List<TMember> members = service.queryList(params);
        TMember member = members.isEmpty() ? null : members.get(0);

        if (member == null) {
            TMember param = new TMember();
            param.setPhone(account);
            param.setPassword(SecurityUtil.encryptPassword(password));
            param.setAvatar(PropertiesUtil.getString("ui.file.uri.prefix") + "extends/img/dftAvatar.png");
            service.update(param);
            return setSuccessModelMap();
        } else {
            throw new IllegalArgumentException("手机号已注册.");
        }
    }

    @PostMapping("login")
    @ApiOperation(value = "登录", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "使用手机号+密码登录\n"
            + "登录接口需要以下四个参数：\n" + "1. UUID(header): 客户端生成的唯一ID，用作用户令牌，服务端用之识别用户，验证权限，每接口必传\n"
            + "2. sign: 请求参数的RSA签名(通过/app/secret.api获取RSA私钥)，用于防止请求参数被第三方拦截篡改，每接口必传。签名算法查看密钥接口\n"
            + "3. account: 用户名，目前只支持手机号\n" + "4. password: 密码\n"
            + "注意：所有接口都需要传UUID和sign参数，UUID用作令牌，sign用作签名", response = TMember.class)
    public Object login(@RequestHeader("UUID") @ApiParam(value = "客户端生成的唯一ID", required = true) String uuid,
        @RequestHeader(required = false) @ApiParam("微信ID，暂不支持") String openId,
        @RequestHeader(required = false) @ApiParam("极光推送ID，暂不支持") String registrationId,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password) {
        Map<String, Object> params = InstanceUtil.newHashMap("enable", 1);
        params.put("loginKey", account);
        List<TMember> members = service.queryList(params);
        TMember member = members.isEmpty() ? null : members.get(0);

        if (member == null) {
            throw new LoginException("手机号或密码错误.");
        } else {
            if (SecurityUtil.encryptPassword(password).equals(member.getPassword())) {
                // if (member.getRegistrationId() != null) {
                // if (registrationId != null && !registrationId.equals(member.getRegistrationId())) {
                // member.setRegistrationId(registrationId);
                // String content = "帐号[" + account + "]在别的设备登录";
                // HashMap<String, String> hashMap = new HashMap<String, String>();
                // hashMap.put("type", "3");
                // String equipment = member.getRegistrationId().substring(2, 3);
                // try {
                // if ("0".equals(equipment)) {
                // jpushHelper.sendNotificationAndroid("登录通知", content, hashMap,
                // member.getRegistrationId());
                // } else {
                // jpushHelper.sendNotificationIOS("登录通知", content, hashMap, member.getRegistrationId());
                // }
                // } catch (Exception e) {
                // logger.info(ExceptionUtil.getStackTraceAsString(e));
                // }
                // }
                // } else {
                // member.setRegistrationId(registrationId);
                // }
                // if (openId != null) {
                // member.setWxOpenId(openId);
                // }

                service.update(member);

                String token = SecurityUtil.initHmacKey(HmacCoder.MD5);
                String tokenKey = SecurityUtil.encryptMd5(token);
                SessionUser user = new SessionUser(member.getId(), member.getNickName(), member.getAvatar(), false);
                CacheUtil.getCache().set(Constants.TOKEN_KEY + tokenKey, user,
                    PropertiesUtil.getInt("APP-TOKEN-EXPIRE", 60 * 60 * 24 * 5));
                member.setToken(token);
                member.setPassword(null);
                return setSuccessModelMap(member);
            } else {
                throw new LoginException("手机号或密码错误.");
            }
        }
    }

    @PostMapping("logout")
    @ApiOperation(value = "APP会员登出", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object logout(HttpServletRequest request) {
        String token = request.getHeader("token");
        Assert.notNull(token, "ACCOUNT");
        if (StringUtils.isNotBlank(token)) {
            String tokenKey = SecurityUtil.encryptMd5(token);
            CacheUtil.getCache().del(Constants.TOKEN_KEY + tokenKey);
        }
        Long id = getCurrUser(request);
        if (DataUtil.isNotEmpty(id)) {
            TMember member = new TMember();
            member.setId(getCurrUser(request));
            service.update(member);
        }
        ModelMap modelMap = new ModelMap();
        return setSuccessModelMap(modelMap);
    }

    @PostMapping("updatePwd")
    @ApiOperation(value = "修改密码", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "接口需要以下五个参数：\n"
            + "1. UUID(header): 客户端生成的唯一ID，用作用户令牌，服务端用之识别用户，验证权限，每接口必传\n"
            + "2. sign: 请求参数的RSA签名(通过/app/secret.api获取RSA私钥)，用于防止请求参数被第三方拦截篡改，每接口必传。签名算法查看密钥接口\n"
            + "3. account: 用户名，目前只支持手机号\n" + "4. password: 密码\n" + "5. authCode: 手机短信验证码(通过/app/msg.api发送短信验证码)\n"
            + "注意：所有接口都需要传UUID和sign参数，UUID用作令牌，sign用作签名", response = TMember.class)
    public Object updatePwd(@RequestHeader("UUID") @ApiParam(value = "客户端生成的唯一ID", required = true) String uuid,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password,
        @RequestParam @ApiParam(value = "手机验证码", required = true) String authCode) throws Exception {
        String authCodeOnServer = (String)CacheUtil.getCache().get(MsgChkType.CHGPWD + account);
        if (!authCode.equals(authCodeOnServer)) {
            throw new IllegalArgumentException("手机验证码错误");
        }

        Map<String, Object> params = InstanceUtil.newHashMap("loginKey", account);
        List<?> members = service.queryList(params);
        TMember member = members.isEmpty() ? null : (TMember)members.get(0);

        if (member == null) {
            throw new IllegalArgumentException("手机号还没有注册.");
        } else {
            member.setPassword(SecurityUtil.encryptPassword(password));
            service.update(member);
            return setSuccessModelMap();
        }
    }

    @PostMapping("wxin")
    @ApiOperation(value = "验证小程序用户信息")
    public Object validOpenId(HttpServletRequest request, String code, String nickName,
        String sex, String avatar, String province, String city) {
        Map<String, Object> param = WebUtil.getParameter(request);
        code = (String)param.get("code");
        org.springframework.util.Assert.hasText(code, "缺少参数");
        org.springframework.util.Assert.hasText(nickName, "缺少参数");
        NameValuePair appid = new NameValuePair("appid", paramService.getValue("WX-XCX-APPID", ""));
        NameValuePair secret = new NameValuePair("secret", paramService.getValue("WX-XCX-SECRET", ""));
        NameValuePair js_code = new NameValuePair("js_code", code);
        NameValuePair grant_type = new NameValuePair("grant_type", paramService.getValue("GRANT-TYPE", ""));
        String httpClientPost = HttpUtil.post(paramService.getValue("WX-SESSION-KEY", null), appid, secret, js_code,
            grant_type);
        WxUserInfo userInfo = JSON.parseObject(httpClientPost, WxUserInfo.class);
        if (userInfo.getErrcode() != null && !"0".equals(userInfo.getErrcode())) {
            throw new BusinessException(userInfo.getErrmsg());
        }
        userInfo.setNickName(nickName).setSex(sex).setAvatar(avatar).setLocationProvince(province)
        .setLocationCity(city);
        TMember result = service.validOpenId(userInfo);
        result.setPassword(null);
        String token = SecurityUtil.initHmacKey(HmacCoder.MD5);
        String tokenKey = SecurityUtil.encryptMd5(token);
        SessionUser user = new SessionUser(result.getId(), result.getNickName(), result.getPhone(), false);
        CacheUtil.getCache().set(Constants.TOKEN_KEY + tokenKey, user,
            PropertiesUtil.getInt("APP-TOKEN-EXPIRE", 60 * 60 * 24));
        result.setToken(token);
        return setSuccessModelMap(result);
    }
}
