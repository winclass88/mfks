package com.mfks.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mfks.model.TMember;
import com.mfks.model.TMemberTest;
import com.mfks.model.TTestTitle;
import com.mfks.service.MemberTestService;
import com.mfks.service.TestTitleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.InstanceUtil;

/**
 * <p>
 * 测试题目  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@RestController
@RequestMapping("/app/testTitle/")
@Api(value = "测试题目接口", description = "测试题目接口")
public class TestTitleController extends AppBaseController<TTestTitle, TestTitleService> {
    private MemberTestService memberTestService;

    @RequestMapping(value = "test", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "分页查询测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object test(HttpServletRequest request, String typeId, Integer index) {
        Map<String, Object> params = InstanceUtil.newHashMap();
        params.put("testType", typeId);
        params.put("pageSize", 1);
        params.put("pageIndex", index == null ? 1 : index);
        Pagination<TTestTitle> page = service.randomTestTitles(params);
        ModelMap modelMap = new ModelMap();
        modelMap.put("data", page.getRecords());
        modelMap.put("current", page.getCurrent());
        modelMap.put("size", page.getSize());
        modelMap.put("pages", page.getPages());
        modelMap.put("total", page.getTotal());
        return super.setSuccessModelMap(modelMap);
    }

    @PostMapping(value = "/submit")
    @ApiOperation(value = "分页查询测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object submit(HttpServletRequest request, String answer) {
        Long userId = getCurrUser(request);
        String[] answers = answer.split("\\@");
        Object result = service.submit(userId, answers);
        ModelMap modelMap = new ModelMap();
        modelMap.put("data", result);
        return super.setSuccessModelMap(modelMap);
    }

    @ApiOperation(value = "获取测评结果", produces = MediaType.APPLICATION_JSON_VALUE, response = TMember.class)
    @RequestMapping(value = "testResult", method = {RequestMethod.GET, RequestMethod.POST})
    public Object getTestResult(HttpServletRequest request, @RequestParam(required = false) String typeId,
        Long memberId) {
        Map<String, Object> params = InstanceUtil.newHashMap();
        params.put("testType", typeId);
        if (memberId == null) {
            memberId = getCurrUser(request);
        }
        params.put("memberId", memberId);
        List<TMemberTest> result = memberTestService.queryList(params);
        ModelMap modelMap = new ModelMap();
        modelMap.put("data", result);
        return setSuccessModelMap(modelMap);
    }
}
