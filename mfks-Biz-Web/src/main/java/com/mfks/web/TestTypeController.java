package com.mfks.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfks.model.TTestType;
import com.mfks.service.TestTypeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 测试类型  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@RestController
@RequestMapping("/app/testType")
@Api(value = "测试类型接口", description = "测试类型接口")
public class TestTypeController extends AppBaseController<TTestType, TestTypeService> {
    @GetMapping(value = "/read/list")
    @ApiOperation(value = "查询全部测试类型", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.queryList(param);
    }
}