package com.mfks.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mfks.bean.Member;
import com.mfks.model.TMemberFavorite;
import com.mfks.service.MemberFavoriteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.WebUtil;

@RestController
@RequestMapping("/app/favorite")
@Api(value = "收藏接口", description = "收藏接口")
public class FavoriteController extends BaseController<TMemberFavorite, MemberFavoriteService> {
    @PostMapping("update")
    @ApiOperation(value = "收藏", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updatePhone(HttpServletRequest request, Long memberId) {
        Long id = getCurrUser(request);
        TMemberFavorite favorite = new TMemberFavorite().setMemberId(id).setFavMemberId(memberId);
        Integer result = service.deleteByEntity(favorite);
        if (result == 0) {
            favorite.setCreateBy(id);
            favorite.setUpdateBy(id);
            service.update(favorite);
        }
        return setSuccessModelMap(new ModelMap());
    }

    @RequestMapping(value = "my", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "我的收藏", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object my(HttpServletRequest request) {
        Long id = getCurrUser(request);
        Map<String, Object> params = WebUtil.getParameter(request);
        params.put("memberId", id);
        Pagination<Member> members = service.queryFavMember(params);
        return setSuccessModelMap(members);
    }

    @RequestMapping(value = "favMe", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "关注我的人", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object favMe(HttpServletRequest request) {
        Long id = getCurrUser(request);
        Map<String, Object> params = WebUtil.getParameter(request);
        params.put("favMemberId", id);
        Pagination<Member> members = service.queryMember(params);
        return setSuccessModelMap(members);
    }
}