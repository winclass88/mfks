package com.mfks.web;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.mfks.model.TMemberOrder;
import com.mfks.service.MemberOrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.support.pay.WxPayment;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 订单信息  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@Controller
@RequestMapping("/app/memberOrder")
@Api(value = "订单信息接口", description = "订单信息接口")
public class MemberOrderController extends AppBaseController<TMemberOrder, MemberOrderService> {
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询订单信息", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("memberId", WebUtil.getCurrentUser(request).getId());
        return super.query(param);
    }

    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "订单信息详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        param.setMemberId(WebUtil.getCurrentUser(request).getId());
        return super.get(param);
    }

    @PostMapping("update")
    @ApiOperation(value = "修改订单信息", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        Long userId = getCurrUser(request);
        param.setMemberId(userId);
        param.setClientIP(WebUtil.getHost(request));
        param.setCreateBy(userId);
        param.setCreateTime(new Date());
        param.setUpdateBy(userId);
        param.setUpdateTime(new Date());
        Object record = service.create(param);
        return setSuccessModelMap(record);
    }

    @RequestMapping(value = "getPayResult", method = {RequestMethod.POST, RequestMethod.GET})
    @ApiOperation(value = "查询支付状态", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object updateState(HttpServletRequest request, String orderNo) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        param.setMemberId(getCurrUser(request));
        Assert.notNull(param.getMemberId(), "用户ID不能为空.");
        Assert.notNull(param.getOrderNo(), "订单ID不能为空.");
        Object result = service.updateOrderState(param);
        return setSuccessModelMap(result);
    }

    @RequestMapping(value = "wxPayReturn.api", method = {RequestMethod.POST})
    @ApiOperation(value = "微信回调", notes = "微信回调接口", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiIgnore
    public Object wxPayReturn(HttpServletRequest request) {
        Map<String, String> result = InstanceUtil.newHashMap("return_code", "<![CDATA[SUCCESS]]>");
        try {
            Map<String, Object> param = WebUtil.getParameter(request);
            Assert.notNull(param.get("appid"), "应用APPID不能为空.");
            Assert.notNull(param.get("mch_id"), "商户号不能为空.");
            service.updateWxPayReturn(param);
        } catch (Exception e) {
            logger.error("", e);
            result = InstanceUtil.newHashMap("return_code", "<![CDATA[FAIL]]>");
            result.put("return_msg", "<![CDATA[" + e.getMessage() + "]]>");
        }
        logger.info(JSON.toJSONString(result));
        return ResponseEntity.ok(WxPayment.toXml(result));
    }
}
