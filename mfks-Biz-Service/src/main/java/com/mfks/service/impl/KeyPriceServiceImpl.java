package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TKeyPriceMapper;
import com.mfks.model.TKeyPrice;
import com.mfks.service.KeyPriceService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 钥匙价格 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Service
@CacheConfig(cacheNames = "TKeyPrice")
public class KeyPriceServiceImpl extends BaseServiceImpl<TKeyPrice, TKeyPriceMapper> implements KeyPriceService {

}
