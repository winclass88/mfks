package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TMemberLifewayMapper;
import com.mfks.model.TMemberLifeway;
import com.mfks.service.MemberLifewayService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.util.CacheUtil;
/**
 * <p>
 * 会员生活方式 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@Service
@CacheConfig(cacheNames = "MemberLifeway")
public class MemberLifewayServiceImpl extends BaseServiceImpl<TMemberLifeway, TMemberLifewayMapper>
implements MemberLifewayService {

    @Override
    public TMemberLifeway update(TMemberLifeway param) {
        CacheUtil.getCache().del(getLockKey("MEMBER:" + param.getMemberId()));
        return super.update(param);
    }

    @Override
    public TMemberLifeway queryByMemberId(Long memberId) {
        TMemberLifeway record = (TMemberLifeway) CacheUtil.getCache().get(getLockKey("MEMBER:" + memberId));
        if (record == null) {
            record = selectOne(new TMemberLifeway().setMemberId(memberId));
            if (record != null) {
                CacheUtil.getCache().set(getLockKey("MEMBER:" + memberId), record);
            }
        }
        return record;
    }
}
