package com.mfks.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.bean.Member;
import com.mfks.bean.WxUserInfo;
import com.mfks.mapper.TMemberMapper;
import com.mfks.model.TMember;
import com.mfks.model.TMemberFavorite;
import com.mfks.model.TMemberInterest;
import com.mfks.model.TMemberLifeway;
import com.mfks.model.TMemberMarriage;
import com.mfks.service.MemberFavoriteService;
import com.mfks.service.MemberInterestService;
import com.mfks.service.MemberLifewayService;
import com.mfks.service.MemberMarriageService;
import com.mfks.service.MemberPhotoService;
import com.mfks.service.MemberService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.DataUtil;
import top.ibase4j.core.util.DateUtil;
import top.ibase4j.core.util.InstanceUtil;

/**
 * <p>
 * 会员 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-10-12
 */
@CacheConfig(cacheNames = "member")
@Service(interfaceClass = MemberService.class)
public class MemberServiceImpl extends BaseServiceImpl<TMember, TMemberMapper> implements MemberService {
    @Autowired
    private MemberInterestService interestService;
    @Autowired
    private MemberLifewayService lifewayService;
    @Autowired
    private MemberMarriageService marriageService;
    @Autowired
    private MemberPhotoService photoService;
    @Autowired
    private MemberFavoriteService favoriteService;

    @Override
    @Transactional
    public TMember updateAll(TMember record) {
        TMemberInterest interest = DataUtil.ifNull(record.getInterest(), new TMemberInterest());
        TMemberLifeway lifeway = DataUtil.ifNull(record.getLifeway(), new TMemberLifeway());
        TMemberMarriage marriage = DataUtil.ifNull(record.getMarriage(), new TMemberMarriage());
        if (record.getId() == null) {
            if (record.getPhone() != null && record.getPhone().length() > 10) {
                record.setUserName("会员" + record.getPhone().substring(7, 10));
                record.setNickName("会员" + record.getPhone().substring(7, 10));
            }
        } else {
            TMemberInterest memberInterest = interestService.queryByMemberId(record.getId());
            if (memberInterest != null) {
                interest.setId(memberInterest.getId());
            }
            TMemberLifeway memberLifeway = lifewayService.queryByMemberId(record.getId());
            if (memberLifeway != null) {
                lifeway.setId(memberLifeway.getId());
            }
            TMemberMarriage memberMarriage = marriageService.queryByMemberId(record.getId());
            if (memberMarriage != null) {
                marriage.setId(memberMarriage.getId());
            }
        }
        if (record.getBirthDay() != null) {
            record.setStarSign(DateUtil.getStarSign(record.getBirthDay()));
        }
        TMember result = super.update(record);
        interest.setMemberId(result.getId());
        interest.setCreateBy(result.getCreateBy());
        interest.setUpdateBy(result.getUpdateBy());
        interestService.update(interest);
        lifeway.setMemberId(result.getId());
        lifeway.setCreateBy(result.getCreateBy());
        lifeway.setUpdateBy(result.getUpdateBy());
        lifewayService.update(lifeway);
        marriage.setMemberId(result.getId());
        marriage.setCreateBy(result.getCreateBy());
        marriage.setUpdateBy(result.getUpdateBy());
        marriageService.update(marriage);

        return result;
    }

    @Override
    public Pagination<Member> queryBaseInfo(Map<String, Object> params) {
        Pagination<TMember> pagination = super.query(params);
        Pagination<Member> page = new Pagination<Member>(pagination.getCurrent(), pagination.getSize());
        for (TMember tMember : pagination.getRecords()) {
            Member member = InstanceUtil.to(tMember, Member.class);
            member.setAge(tMember.getAge());
            page.getRecords().add(member);
        }
        return page;
    }

    @Override
    public TMember getInfo(Long id) {
        TMember member = super.queryById(id);
        if (member != null) {
            member.setPassword(null);
            member.setUuid(null);
            member.setToken(null);
        }
        member.setInterest(interestService.queryByMemberId(member.getId()));
        member.setLifeway(lifewayService.queryByMemberId(member.getId()));
        member.setMarriage(marriageService.queryByMemberId(member.getId()));
        member.setPhotos(photoService
            .query(InstanceUtil.newHashMap(new String[]{"memberId", "pageSize"}, new Object[]{member.getId(), "4"}))
            .getRecords());
        member.setCollectedNum(favoriteService.count(new TMemberFavorite().setFavMemberId(member.getId())));
        return member;
    }

    @Override
    public Object updataphone(Map<String, Object> map) {
        TMember member1 = new TMember();
        member1.setPhone((String)map.get("orderPhone"));
        TMember member2 = super.selectOne(member1);
        if (member2.getId() == null) {
            return "请输入正确的信息 ";
        } else {
            member2.setPhone((String)map.get("newPhone"));
            return super.update(member2);
        }
    }

    @Override
    public Object authentication(Map<String, Object> map) {// 实名认证
        TMember MEMBER = new TMember();
        MEMBER.setId(Long.parseLong((String)map.get("memberId")));
        TMember selectOne = super.selectOne(MEMBER);
        return super.update(selectOne);
    }

    @Override
    public TMember validOpenId(WxUserInfo info) {
        TMember tMember = new TMember();
        tMember.setWxOpenid(info.getOpenid());
        TMember selectOne = super.selectOne(tMember);
        if (selectOne == null) {
            tMember = InstanceUtil.to(info, TMember.class);
            tMember.setWxOpenid(info.getOpenid());
            return updateAll(tMember);
        } else {
            return queryById(selectOne.getId());
        }
    }
}
