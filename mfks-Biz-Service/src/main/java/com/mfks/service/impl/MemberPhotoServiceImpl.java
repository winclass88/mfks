package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TMemberPhotoMapper;
import com.mfks.model.TMemberPhoto;
import com.mfks.service.MemberPhotoService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 会员头像 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@Service
@CacheConfig(cacheNames = "MemberPhoto")
public class MemberPhotoServiceImpl extends BaseServiceImpl<TMemberPhoto, TMemberPhotoMapper>
implements MemberPhotoService {

}
