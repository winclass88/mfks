package com.mfks.service.impl;

import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TTestResultMapper;
import com.mfks.model.TTestResult;
import com.mfks.service.TestResultService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;

/**
 * <p>
 * 测试结果 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Service
@CacheConfig(cacheNames = "TTestResult")
public class TestResultServiceImpl extends BaseServiceImpl<TTestResult, TTestResultMapper>
implements TestResultService {
    @Override
    public Pagination<TTestResult> query(Map<String, Object> params) {
        params.put("asc", "test_type, test_group");
        params.put("desc", "id_");
        return super.query(params);
    }
}
