package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TMemberInterestMapper;
import com.mfks.model.TMemberInterest;
import com.mfks.service.MemberInterestService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.util.CacheUtil;
/**
 * <p>
 * 会员兴趣爱好 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@Service
@CacheConfig(cacheNames = "MemberInterest")
public class MemberInterestServiceImpl extends BaseServiceImpl<TMemberInterest, TMemberInterestMapper>
implements MemberInterestService {

    @Override
    public TMemberInterest update(TMemberInterest param) {
        CacheUtil.getCache().del(getLockKey("MEMBER:" + param.getMemberId()));
        return super.update(param);
    }

    @Override
    public TMemberInterest queryByMemberId(Long memberId) {
        TMemberInterest record = (TMemberInterest) CacheUtil.getCache().get(getLockKey("MEMBER:" + memberId));
        if (record == null) {
            record = selectOne(new TMemberInterest().setMemberId(memberId));
            if (record != null) {
                CacheUtil.getCache().set(getLockKey("MEMBER:" + memberId), record);
            }
        }
        return record;
    }
}
