package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TTestTypeMapper;
import com.mfks.model.TTestType;
import com.mfks.service.TestTypeService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 测试类型 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Service
@CacheConfig(cacheNames = "TTestType")
public class TestTypeServiceImpl extends BaseServiceImpl<TTestType, TTestTypeMapper> implements TestTypeService {

}
