package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TMemberTestMapper;
import com.mfks.model.TMemberTest;
import com.mfks.service.MemberTestService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 * 用户测试 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2019-01-24
 */
@Service
@CacheConfig(cacheNames = "TMemberTest")
public class MemberTestServiceImpl extends BaseServiceImpl<TMemberTest, TMemberTestMapper>
implements MemberTestService {

}
