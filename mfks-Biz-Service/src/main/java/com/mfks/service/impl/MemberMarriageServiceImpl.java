package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TMemberMarriageMapper;
import com.mfks.model.TMemberMarriage;
import com.mfks.service.MemberMarriageService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.util.CacheUtil;
/**
 * <p>
 * 会员婚姻观念 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@Service
@CacheConfig(cacheNames = "MemberMarriage")
public class MemberMarriageServiceImpl extends BaseServiceImpl<TMemberMarriage, TMemberMarriageMapper>
implements MemberMarriageService {

    @Override
    public TMemberMarriage update(TMemberMarriage param) {
        CacheUtil.getCache().del(getLockKey("MEMBER:" + param.getMemberId()));
        return super.update(param);
    }

    @Override
    public TMemberMarriage queryByMemberId(Long memberId) {
        TMemberMarriage record = (TMemberMarriage) CacheUtil.getCache().get(getLockKey("MEMBER:" + memberId));
        if (record == null) {
            record = selectOne(new TMemberMarriage().setMemberId(memberId));
            if (record != null) {
                CacheUtil.getCache().set(getLockKey("MEMBER:" + memberId), record);
            }
        }
        return record;
    }
}