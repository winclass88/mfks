package com.mfks.service.impl;

import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.bean.Member;
import com.mfks.mapper.TMemberFavoriteMapper;
import com.mfks.model.TMember;
import com.mfks.model.TMemberFavorite;
import com.mfks.service.MemberFavoriteService;
import com.mfks.service.MemberService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.InstanceUtil;
/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@Service
@CacheConfig(cacheNames = "MemberFavorite")
public class MemberFavoriteServiceImpl extends BaseServiceImpl<TMemberFavorite, TMemberFavoriteMapper>
implements MemberFavoriteService {
    private MemberService memberService;

    @Override
    public Pagination<Member> queryFavMember(Map<String, Object> params) {
        Pagination<TMemberFavorite> pagination = super.query(params);
        Pagination<Member> result = new Pagination<>(pagination.getCurrent(), pagination.getSize());
        result.setTotal(pagination.getTotal());
        for (TMemberFavorite favorite : pagination.getRecords()) {
            TMember member = memberService.queryById(favorite.getFavMemberId());
            result.getRecords().add(InstanceUtil.to(member, Member.class));
        }
        return result;
    }

    @Override
    public Pagination<Member> queryMember(Map<String, Object> params) {
        Pagination<TMemberFavorite> pagination = super.query(params);
        Pagination<Member> result = new Pagination<>(pagination.getCurrent(), pagination.getSize());
        result.setTotal(pagination.getTotal());
        for (TMemberFavorite favorite : pagination.getRecords()) {
            TMember member = memberService.queryById(favorite.getMemberId());
            result.getRecords().add(InstanceUtil.to(member, Member.class));
        }
        return result;
    }
}
