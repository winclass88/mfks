package com.mfks.service.impl;

import java.util.Map;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.TTestOptionMapper;
import com.mfks.model.TTestOption;
import com.mfks.service.TestOptionService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
/**
 * <p>
 * 测试题目选项 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Service
@CacheConfig(cacheNames = "TTestOption")
public class TestOptionServiceImpl extends BaseServiceImpl<TTestOption, TTestOptionMapper>
implements TestOptionService {
    @Override
    public Pagination<TTestOption> query(Map<String, Object> params) {
        params.put("asc", "test_id, option_key");
        params.put("desc", "id_");
        return super.query(params);
    }
}
