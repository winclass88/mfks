package com.mfks.mapper;

import com.mfks.model.TTestType;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TTestTypeMapper extends BaseMapper<TTestType> {

}