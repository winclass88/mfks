package com.mfks.mapper;

import com.mfks.model.TTestOption;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TTestOptionMapper extends BaseMapper<TTestOption> {

}