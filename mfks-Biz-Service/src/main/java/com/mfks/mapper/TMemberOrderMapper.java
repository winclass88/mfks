package com.mfks.mapper;

import com.mfks.model.TMemberOrder;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
public interface TMemberOrderMapper extends BaseMapper<TMemberOrder> {

}