package com.mfks.mapper;

import com.mfks.model.TMemberTest;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2019-01-24
 */
public interface TMemberTestMapper extends BaseMapper<TMemberTest> {

}