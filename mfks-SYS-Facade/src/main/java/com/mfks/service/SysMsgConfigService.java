package com.mfks.service;

import com.mfks.model.SysMsgConfig;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:58:43
 */
public interface SysMsgConfigService extends BaseService<SysMsgConfig> {

}
