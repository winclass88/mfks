package com.mfks.service;

import com.mfks.model.SysEmailConfig;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailConfigService extends BaseService<SysEmailConfig> {

}
