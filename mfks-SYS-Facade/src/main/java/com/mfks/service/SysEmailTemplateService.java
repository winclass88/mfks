package com.mfks.service;

import com.mfks.model.SysEmailTemplate;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailTemplateService extends BaseService<SysEmailTemplate> {

}
