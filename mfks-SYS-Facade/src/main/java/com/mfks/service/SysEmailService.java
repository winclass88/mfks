package com.mfks.service;

import com.mfks.model.SysEmail;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailService extends BaseService<SysEmail> {

}
