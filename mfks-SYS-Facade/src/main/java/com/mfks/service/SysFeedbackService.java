package com.mfks.service;

import com.mfks.model.SysFeedback;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:55:44
 */
public interface SysFeedbackService extends BaseService<SysFeedback> {

}
