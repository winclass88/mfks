package com.mfks.service;

import com.mfks.model.SysUnit;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:28:59
 */
public interface SysUnitService extends BaseService<SysUnit> {

}
