package com.mfks.service;

import com.mfks.model.SysNotice;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysNoticeService extends BaseService<SysNotice> {

}
