package com.mfks.service;

import com.mfks.model.SysDept;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午10:59:30
 */
public interface SysDeptService extends BaseService<SysDept> {

}
