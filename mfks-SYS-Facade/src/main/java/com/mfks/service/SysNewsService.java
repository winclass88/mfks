package com.mfks.service;

import com.mfks.model.SysNews;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysNewsService extends BaseService<SysNews> {

}
