package com.mfks.service;

import com.mfks.model.SysArticle;

import top.ibase4j.core.base.BaseService;

/**
 * 文章  服务
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:43:15
 */
public interface SysArticleService extends BaseService<SysArticle> {

}
