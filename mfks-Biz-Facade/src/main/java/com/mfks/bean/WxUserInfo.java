package com.mfks.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class WxUserInfo implements Serializable{

    private String session_key;//会话密钥

    private String expires_in;//

    private String openid;//用户唯一标识

    private String errcode;//错误码
    private String errmsg;

    private String nickName;
    private String sex;
    private String avatar;
    private String locationProvince;
    private String locationCity;

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getNickName() {
        return nickName;
    }

    public WxUserInfo setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public WxUserInfo setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public WxUserInfo setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public String getLocationProvince() {
        return locationProvince;
    }

    public WxUserInfo setLocationProvince(String locationProvince) {
        this.locationProvince = locationProvince;
        return this;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public WxUserInfo setLocationCity(String locationCity) {
        this.locationCity = locationCity;
        return this;
    }
}
