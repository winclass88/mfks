package com.mfks.bean;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
public class Member implements Serializable {
    private Long id;
    private String nickName;
    private String avatar;
    private String sex;
    private Integer age;
    @ApiModelProperty(value = "祖籍省市")
    private String familyProvince;
    @ApiModelProperty(value = "祖籍市")
    private String familyCity;
    @ApiModelProperty(value = "身高")
    private Integer stature;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getFamilyProvince() {
        return familyProvince;
    }

    public void setFamilyProvince(String familyProvince) {
        this.familyProvince = familyProvince;
    }

    public String getFamilyCity() {
        return familyCity;
    }

    public void setFamilyCity(String familyCity) {
        this.familyCity = familyCity;
    }

    public Integer getStature() {
        return stature;
    }

    public void setStature(Integer stature) {
        this.stature = stature;
    }
}
