package com.mfks.model;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 测试题目选项
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@ApiModel("测试题目选项")
@TableName("t_test_option")
@SuppressWarnings("serial")
public class TTestOption extends BaseModel {

    @ApiModelProperty(value = "测试题目")
    @TableField("test_id")
    private Long testId;
    @ApiModelProperty(value = "测试选项")
    @TableField("test_option")
    private String testOption;
    @ApiModelProperty(value = "选项编号")
    @TableField("option_key")
    private String optionKey;
    @ApiModelProperty(value = "选项分值")
    @TableField("option_val")
    private BigDecimal optionVal;

    public Long getTestId() {
        return testId;
    }

    public TTestOption setTestId(Long testId) {
        this.testId = testId;
        return this;
    }

    public String getTestOption() {
        return testOption;
    }

    public TTestOption setTestOption(String testOption) {
        this.testOption = testOption;
        return this;
    }

    public String getOptionKey() {
        return optionKey;
    }

    public TTestOption setOptionKey(String optionKey) {
        this.optionKey = optionKey;
        return this;
    }

    public BigDecimal getOptionVal() {
        return optionVal;
    }

    public TTestOption setOptionVal(BigDecimal optionVal) {
        this.optionVal = optionVal;
        return this;
    }
}