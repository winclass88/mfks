package com.mfks.model;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;
import top.ibase4j.core.util.DateUtil;

/**
 * <p>
 * 会员
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@ApiModel("会员")
@TableName("t_member")
@SuppressWarnings("serial")
public class TMember extends BaseModel {

    @ApiModelProperty(value = "姓名")
    @TableField("user_name")
    private String userName;
    @ApiModelProperty(value = "密码")
    @TableField("password_")
    private String password;
    @ApiModelProperty(value = "电话")
    @TableField("phone_")
    private String phone;
    @ApiModelProperty(value = "昵称")
    @TableField("nick_name")
    private String nickName;
    @ApiModelProperty(value = "性别(1:男;2:女)")
    @TableField("sex_")
    private String sex;
    @ApiModelProperty(value = "头像")
    @TableField("avatar_")
    private String avatar;
    @ApiModelProperty(value = "个性签名")
    @TableField("personal_sign")
    private String personalSign;
    @ApiModelProperty(value = "证件类型")
    @TableField("card_type")
    private String cardType;
    @ApiModelProperty(value = "证件号码")
    @TableField("card_no")
    private String cardNo;
    @ApiModelProperty(value = "出生日期")
    @TableField("birth_day")
    private Date birthDay;
    @ApiModelProperty(value = "年龄")
    @TableField("age_")
    private Integer age;
    @ApiModelProperty(value = "民族")
    @TableField("nation_")
    private String nation;
    @ApiModelProperty(value = "文化程度")
    @TableField("education_")
    private String education;
    @ApiModelProperty(value = "身高")
    @TableField("stature_")
    private Integer stature;
    @ApiModelProperty(value = "血型")
    @TableField("blood_type")
    private String bloodType;
    @ApiModelProperty(value = "星座")
    @TableField("star_sign")
    private String starSign;
    @ApiModelProperty(value = "职业")
    @TableField("vocation_")
    private String vocation;
    @ApiModelProperty(value = "祖籍省")
    @TableField("family_province")
    private String familyProvince;
    @ApiModelProperty(value = "祖籍市")
    @TableField("family_city")
    private String familyCity;
    @ApiModelProperty(value = "所在省")
    @TableField("location_province")
    private String locationProvince;
    @ApiModelProperty(value = "所在市")
    @TableField("location_city")
    private String locationCity;
    @ApiModelProperty(value = "婚姻状况")
    @TableField("marital_status")
    private String maritalStatus;
    @ApiModelProperty(value = "有无子女")
    @TableField("children_status")
    private String childrenStatus;
    @ApiModelProperty(value = "居住情况")
    @TableField("living_condition")
    private String livingCondition;
    @ApiModelProperty(value = "购车情况")
    @TableField("car_situation")
    private String carSituation;
    @ApiModelProperty(value = "交友状态")
    @TableField("friendship_state")
    private String friendshipState;
    @ApiModelProperty(value = "微信唯一标识")
    @TableField("wx_openId")
    private String wxOpenid;
    @ApiModelProperty(value = "客户端唯一标识")
    @TableField("uuid_")
    private String uuid;
    @ApiModelProperty(value = "APP会话token")
    @TableField("token_")
    private String token;
    @ApiModelProperty(value = "钥匙数量")
    @TableField("key_num")
    private Integer keyNum;
    @ApiModelProperty(value = "差评数量")
    @TableField("bad_review_num")
    private Integer badReviewNum;
    @ApiModelProperty(value = "差评数量")
    @TableField("wx_no")
    private String wxNo;

    @TableField(exist = false)
    private Integer collectedNum;
    @TableField(exist = false)
    private TMemberInterest interest;
    @TableField(exist = false)
    private TMemberLifeway lifeway;
    @TableField(exist = false)
    private TMemberMarriage marriage;
    @TableField(exist = false)
    private List<TMemberPhoto> photos;

    public String getUserName() {
        return userName;
    }

    public TMember setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public TMember setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public TMember setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getNickName() {
        return nickName;
    }

    public TMember setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public TMember setSex(String sex) {
        this.sex = sex;
        return this;
    }

    public String getAvatar() {
        return avatar;
    }

    public TMember setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public String getPersonalSign() {
        return personalSign;
    }

    public TMember setPersonalSign(String personalSign) {
        this.personalSign = personalSign;
        return this;
    }

    public String getCardType() {
        return cardType;
    }

    public TMember setCardType(String cardType) {
        this.cardType = cardType;
        return this;
    }

    public String getCardNo() {
        return cardNo;
    }

    public TMember setCardNo(String cardNo) {
        this.cardNo = cardNo;
        return this;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public TMember setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public String getNation() {
        return nation;
    }

    public TMember setNation(String nation) {
        this.nation = nation;
        return this;
    }

    public String getEducation() {
        return education;
    }

    public TMember setEducation(String education) {
        this.education = education;
        return this;
    }

    public Integer getStature() {
        return stature;
    }

    public TMember setStature(Integer stature) {
        this.stature = stature;
        return this;
    }

    public String getBloodType() {
        return bloodType;
    }

    public TMember setBloodType(String bloodType) {
        this.bloodType = bloodType;
        return this;
    }

    public String getVocation() {
        return vocation;
    }

    public TMember setVocation(String vocation) {
        this.vocation = vocation;
        return this;
    }

    public String getFamilyProvince() {
        return familyProvince;
    }

    public TMember setFamilyProvince(String familyProvince) {
        this.familyProvince = familyProvince;
        return this;
    }

    public String getFamilyCity() {
        return familyCity;
    }

    public TMember setFamilyCity(String familyCity) {
        this.familyCity = familyCity;
        return this;
    }

    public String getLocationProvince() {
        return locationProvince;
    }

    public TMember setLocationProvince(String locationProvince) {
        this.locationProvince = locationProvince;
        return this;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public TMember setLocationCity(String locationCity) {
        this.locationCity = locationCity;
        return this;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public TMember setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public String getChildrenStatus() {
        return childrenStatus;
    }

    public TMember setChildrenStatus(String childrenStatus) {
        this.childrenStatus = childrenStatus;
        return this;
    }

    public String getLivingCondition() {
        return livingCondition;
    }

    public TMember setLivingCondition(String livingCondition) {
        this.livingCondition = livingCondition;
        return this;
    }

    public String getStarSign() {
        return starSign;
    }

    public TMember setStarSign(String starSign) {
        this.starSign = starSign;
        return this;
    }

    public String getCarSituation() {
        return carSituation;
    }

    public TMember setCarSituation(String carSituation) {
        this.carSituation = carSituation;
        return this;
    }

    public String getFriendshipState() {
        return friendshipState;
    }

    public TMember setFriendshipState(String friendshipState) {
        this.friendshipState = friendshipState;
        return this;
    }

    public String getWxOpenid() {
        return wxOpenid;
    }

    public TMember setWxOpenid(String wxOpenid) {
        this.wxOpenid = wxOpenid;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public TMember setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getToken() {
        return token;
    }

    public TMember setToken(String token) {
        this.token = token;
        return this;
    }

    public TMemberInterest getInterest() {
        return interest;
    }

    public void setInterest(TMemberInterest interest) {
        this.interest = interest;
    }

    public TMemberLifeway getLifeway() {
        return lifeway;
    }

    public void setLifeway(TMemberLifeway lifeway) {
        this.lifeway = lifeway;
    }

    public TMemberMarriage getMarriage() {
        return marriage;
    }

    public void setMarriage(TMemberMarriage marriage) {
        this.marriage = marriage;
    }

    public Integer getKeyNum() {
        return keyNum;
    }

    public String getWxNo() {
        return wxNo;
    }

    public void setWxNo(String wxNo) {
        this.wxNo = wxNo;
    }

    public void setKeyNum(Integer keyNum) {
        this.keyNum = keyNum;
    }

    public Integer getAge() {
        if (birthDay == null) {
            return age;
        }
        return DateUtil.getMonthBetween(birthDay, new Date()) / 12;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getBadReviewNum() {
        return badReviewNum;
    }

    public void setBadReviewNum(Integer badReviewNum) {
        this.badReviewNum = badReviewNum;
    }

    public Integer getCollectedNum() {
        return collectedNum;
    }

    public void setCollectedNum(Integer collectedNum) {
        this.collectedNum = collectedNum;
    }

    public List<TMemberPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<TMemberPhoto> photos) {
        this.photos = photos;
    }
}