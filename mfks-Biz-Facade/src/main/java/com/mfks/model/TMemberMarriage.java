package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 会员婚姻观念
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@ApiModel("会员婚姻观念")
@TableName("t_member_marriage")
@SuppressWarnings("serial")
public class TMemberMarriage extends BaseModel {

    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "国家")
    @TableField("country_")
    private String country;
    @ApiModelProperty(value = "省市")
    @TableField("province_")
    private String province;
    @ApiModelProperty(value = "对方脾气秉性与自己")
    @TableField("temper_")
    private String temper;
    @ApiModelProperty(value = "在家里是(情与理)")
    @TableField("heart_minds")
    private String heartMinds;
    @ApiModelProperty(value = "两人相处最重要的是")
    @TableField("together_important")
    private String togetherImportant;
    @ApiModelProperty(value = "你允许哪种开小差形式")
    @TableField("defect_style")
    private String defectStyle;
    @ApiModelProperty(value = "是否要孩子")
    @TableField("want_children")
    private String wantChildren;
    @ApiModelProperty(value = "父母同住")
    @TableField("parent_together")
    private String parentTogether;
    @ApiModelProperty(value = "家庭经济")
    @TableField("family_economy")
    private String familyEconomy;
    @ApiModelProperty(value = "住房要求")
    @TableField("house_")
    private String house;
    @ApiModelProperty(value = "收入要求")
    @TableField("income_")
    private String income;


    public Long getMemberId() {
        return memberId;
    }

    public TMemberMarriage setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public TMemberMarriage setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getProvince() {
        return province;
    }

    public TMemberMarriage setProvince(String province) {
        this.province = province;
        return this;
    }

    public String getHeartMinds() {
        return heartMinds;
    }

    public void setHeartMinds(String heartMinds) {
        this.heartMinds = heartMinds;
    }

    public String getTogetherImportant() {
        return togetherImportant;
    }

    public void setTogetherImportant(String togetherImportant) {
        this.togetherImportant = togetherImportant;
    }

    public String getDefectStyle() {
        return defectStyle;
    }

    public void setDefectStyle(String defectStyle) {
        this.defectStyle = defectStyle;
    }

    public String getTemper() {
        return temper;
    }

    public TMemberMarriage setTemper(String temper) {
        this.temper = temper;
        return this;
    }

    public String getWantChildren() {
        return wantChildren;
    }

    public TMemberMarriage setWantChildren(String wantChildren) {
        this.wantChildren = wantChildren;
        return this;
    }

    public String getParentTogether() {
        return parentTogether;
    }

    public TMemberMarriage setParentTogether(String parentTogether) {
        this.parentTogether = parentTogether;
        return this;
    }

    public String getFamilyEconomy() {
        return familyEconomy;
    }

    public TMemberMarriage setFamilyEconomy(String familyEconomy) {
        this.familyEconomy = familyEconomy;
        return this;
    }

    public String getHouse() {
        return house;
    }

    public TMemberMarriage setHouse(String house) {
        this.house = house;
        return this;
    }

    public String getIncome() {
        return income;
    }

    public TMemberMarriage setIncome(String income) {
        this.income = income;
        return this;
    }

}