package com.mfks.model;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 用户测试
 * </p>
 *
 * @author ShenHuaJie
 * @since 2019-01-24
 */
@ApiModel("用户测试")
@TableName("t_member_test")
@SuppressWarnings("serial")
public class TMemberTest extends BaseModel {

    @ApiModelProperty(value = "用户id")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "测试类型")
    @TableField("test_type")
    private String testType;
    @ApiModelProperty(value = "测试分组")
    @TableField("test_group")
    private String testGroup;
    @ApiModelProperty(value = "测试得分")
    @TableField("test_val")
    private BigDecimal testVal;
    @ApiModelProperty(value = "测试结果")
    @TableField("test_result")
    private String testResult;
    @ApiModelProperty(value = "结果解释")
    @TableField("result_info")
    private String resultInfo;


    public Long getMemberId() {
        return memberId;
    }

    public TMemberTest setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getTestType() {
        return testType;
    }

    public TMemberTest setTestType(String testType) {
        this.testType = testType;
        return this;
    }

    public String getTestGroup() {
        return testGroup;
    }

    public TMemberTest setTestGroup(String testGroup) {
        this.testGroup = testGroup;
        return this;
    }

    public BigDecimal getTestVal() {
        return testVal;
    }

    public TMemberTest setTestVal(BigDecimal testVal) {
        this.testVal = testVal;
        return this;
    }

    public String getTestResult() {
        return testResult;
    }

    public TMemberTest setTestResult(String testResult) {
        this.testResult = testResult;
        return this;
    }

    public String getResultInfo() {
        return resultInfo;
    }

    public void setResultInfo(String resultInfo) {
        this.resultInfo = resultInfo;
    }
}