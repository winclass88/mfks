package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 收藏
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@ApiModel("收藏")
@TableName("t_member_favorite")
@SuppressWarnings("serial")
public class TMemberFavorite extends BaseModel {

    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "会员")
    @TableField("fav_member_id")
    private Long favMemberId;


    public Long getMemberId() {
        return memberId;
    }

    public TMemberFavorite setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public Long getFavMemberId() {
        return favMemberId;
    }

    public TMemberFavorite setFavMemberId(Long favMemberId) {
        this.favMemberId = favMemberId;
        return this;
    }
}