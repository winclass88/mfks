package com.mfks.model;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 钥匙价格
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@ApiModel("钥匙价格")
@TableName("t_key_price")
@SuppressWarnings("serial")
public class TKeyPrice extends BaseModel {

    @ApiModelProperty(value = "钥匙数量")
	@TableField("key_num")
	private Integer keyNum;
    @ApiModelProperty(value = "支付金额")
	@TableField("pay_money")
	private BigDecimal payMoney;


	public Integer getKeyNum() {
		return keyNum;
	}

	public TKeyPrice setKeyNum(Integer keyNum) {
		this.keyNum = keyNum;
		return this;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public TKeyPrice setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
		return this;
	}

}