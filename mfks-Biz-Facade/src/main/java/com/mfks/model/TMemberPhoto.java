package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 会员头像
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@ApiModel("会员头像")
@TableName("t_member_photo")
@SuppressWarnings("serial")
public class TMemberPhoto extends BaseModel {

    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "头像")
    @TableField("photo_")
    private String photo;


    public Long getMemberId() {
        return memberId;
    }

    public TMemberPhoto setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getPhoto() {
        return photo;
    }

    public TMemberPhoto setPhoto(String photo) {
        this.photo = photo;
        return this;
    }

}