package com.mfks.model;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 测试题目
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@ApiModel("测试题目")
@TableName("t_test_title")
@SuppressWarnings("serial")
public class TTestTitle extends BaseModel {

    @ApiModelProperty(value = "测试类型")
    @TableField("test_type")
    private String testType;
    @ApiModelProperty(value = "题目类型(1单选2多选3填空题)")
    @TableField("title_type")
    private String titleType;
    @ApiModelProperty(value = "测试题目")
    @TableField("test_title")
    private String testTitle;
    @TableField("img_url")
    private String imgUrl;
    @ApiModelProperty(value = "题目序号")
    @TableField("title_seq")
    private String titleSeq;
    @ApiModelProperty(value = "题目分组")
    @TableField("title_group")
    private String titleGroup;
    @ApiModelProperty(value = "题目答案")
    @TableField("title_answer")
    private String titleAnswer;
    @ApiModelProperty(value = "题目分值")
    @TableField("title_val")
    private BigDecimal titleVal;

    @TableField(exist = false)
    private List<TTestOption> options;

    public String getTestType() {
        return testType;
    }

    public TTestTitle setTestType(String testType) {
        this.testType = testType;
        return this;
    }

    public String getTestTitle() {
        return testTitle;
    }

    public TTestTitle setTestTitle(String testTitle) {
        this.testTitle = testTitle;
        return this;
    }

    public String getTitleGroup() {
        return titleGroup;
    }

    public void setTitleGroup(String titleGroup) {
        this.titleGroup = titleGroup;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public TTestTitle setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getTitleType() {
        return titleType;
    }

    public TTestTitle setTitleType(String titleType) {
        this.titleType = titleType;
        return this;
    }

    public String getTitleAnswer() {
        return titleAnswer;
    }

    public TTestTitle setTitleAnswer(String titleAnswer) {
        this.titleAnswer = titleAnswer;
        return this;
    }

    public BigDecimal getTitleVal() {
        return titleVal;
    }

    public TTestTitle setTitleVal(BigDecimal titleVal) {
        this.titleVal = titleVal;
        return this;
    }

    public String getTitleSeq() {
        return titleSeq;
    }

    public void setTitleSeq(String titleSeq) {
        this.titleSeq = titleSeq;
    }

    public List<TTestOption> getOptions() {
        return options;
    }

    public void setOptions(List<TTestOption> options) {
        this.options = options;
    }
}