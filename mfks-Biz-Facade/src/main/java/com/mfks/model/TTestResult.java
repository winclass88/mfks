package com.mfks.model;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 测试结果
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@ApiModel("测试结果")
@TableName("t_test_result")
@SuppressWarnings("serial")
public class TTestResult extends BaseModel {

    @ApiModelProperty(value = "测试类型")
    @TableField("test_type")
    private String testType;
    @ApiModelProperty(value = "测试组")
    @TableField("test_group")
    private String testGroup;
    @ApiModelProperty(value = "最小分值")
    @TableField("val_min")
    private BigDecimal valMin;
    @ApiModelProperty(value = "最大分值")
    @TableField("val_max")
    private BigDecimal valMax;
    @ApiModelProperty(value = "测试结果")
    @TableField("result_title")
    private String resultTitle;
    @ApiModelProperty(value = "结果解释")
    @TableField("result_info")
    private String resultInfo;

    @TableField(exist = false)
    private BigDecimal val;

    public String getTestType() {
        return testType;
    }

    public TTestResult setTestType(String testType) {
        this.testType = testType;
        return this;
    }

    public String getTestGroup() {
        return testGroup;
    }

    public TTestResult setTestGroup(String testGroup) {
        this.testGroup = testGroup;
        return this;
    }

    public BigDecimal getValMin() {
        return valMin;
    }

    public TTestResult setValMin(BigDecimal valMin) {
        this.valMin = valMin;
        return this;
    }

    public BigDecimal getValMax() {
        return valMax;
    }

    public TTestResult setValMax(BigDecimal valMax) {
        this.valMax = valMax;
        return this;
    }

    public String getResultTitle() {
        return resultTitle;
    }

    public TTestResult setResultTitle(String resultTitle) {
        this.resultTitle = resultTitle;
        return this;
    }

    public String getResultInfo() {
        return resultInfo;
    }

    public TTestResult setResultInfo(String resultInfo) {
        this.resultInfo = resultInfo;
        return this;
    }

    public BigDecimal getVal() {
        return val;
    }

    public void setVal(BigDecimal val) {
        this.val = val;
    }

}