package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 会员兴趣爱好
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@ApiModel("会员兴趣爱好")
@TableName("t_member_interest")
@SuppressWarnings("serial")
public class TMemberInterest extends BaseModel {

    @ApiModelProperty(value = "会员")
	@TableField("member_id")
	private Long memberId;
    @ApiModelProperty(value = "食物")
	@TableField("food_")
	private String food;
    @ApiModelProperty(value = "书籍")
	@TableField("book_")
	private String book;
    @ApiModelProperty(value = "影视")
	@TableField("film_")
	private String film;
    @ApiModelProperty(value = "节目")
	@TableField("program_")
	private String program;
    @ApiModelProperty(value = "爱好")
	@TableField("hobby_")
	private String hobby;
    @ApiModelProperty(value = "娱乐")
	@TableField("amusement_")
	private String amusement;
    @ApiModelProperty(value = "旅游")
	@TableField("tour_")
	private String tour;


	public Long getMemberId() {
		return memberId;
	}

	public TMemberInterest setMemberId(Long memberId) {
		this.memberId = memberId;
		return this;
	}

	public String getFood() {
		return food;
	}

	public TMemberInterest setFood(String food) {
		this.food = food;
		return this;
	}

	public String getBook() {
		return book;
	}

	public TMemberInterest setBook(String book) {
		this.book = book;
		return this;
	}

	public String getFilm() {
		return film;
	}

	public TMemberInterest setFilm(String film) {
		this.film = film;
		return this;
	}

	public String getProgram() {
		return program;
	}

	public TMemberInterest setProgram(String program) {
		this.program = program;
		return this;
	}

	public String getHobby() {
		return hobby;
	}

	public TMemberInterest setHobby(String hobby) {
		this.hobby = hobby;
		return this;
	}

	public String getAmusement() {
		return amusement;
	}

	public TMemberInterest setAmusement(String amusement) {
		this.amusement = amusement;
		return this;
	}

	public String getTour() {
		return tour;
	}

	public TMemberInterest setTour(String tour) {
		this.tour = tour;
		return this;
	}

}