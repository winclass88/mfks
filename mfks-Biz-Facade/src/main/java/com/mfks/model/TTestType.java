package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 测试类型
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@ApiModel("测试类型")
@TableName("t_test_type")
@SuppressWarnings("serial")
public class TTestType extends BaseModel {

    @ApiModelProperty(value = "测试类型")
	@TableField("test_type")
	private String testType;
	@TableField("img_url")
	private String imgUrl;


	public String getTestType() {
		return testType;
	}

	public TTestType setTestType(String testType) {
		this.testType = testType;
		return this;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public TTestType setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
		return this;
	}

}