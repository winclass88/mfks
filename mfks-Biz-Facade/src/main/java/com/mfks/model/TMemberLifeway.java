package com.mfks.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 会员生活方式
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@ApiModel("会员生活方式")
@TableName("t_member_lifeway")
@SuppressWarnings("serial")
public class TMemberLifeway extends BaseModel {

    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "卫生习惯")
    @TableField("health_habit ")
    private String healthHabit;
    @ApiModelProperty(value = "吸烟")
    @TableField("smoke_")
    private String smoke;
    @ApiModelProperty(value = "饮酒")
    @TableField("drink_")
    private String drink;
    @ApiModelProperty(value = "锻炼")
    @TableField("exercise_")
    private String exercise;
    @ApiModelProperty(value = "饮食")
    @TableField("diet_")
    private String diet;
    @ApiModelProperty(value = "宗教信仰")
    @TableField("religion_")
    private String religion;
    @ApiModelProperty(value = "作息时间")
    @TableField("work_rest")
    private String workRest;
    @ApiModelProperty(value = "收入水平")
    @TableField("income_lvl")
    private String incomeLvl;
    @ApiModelProperty(value = "消费水平")
    @TableField("consumption_lvl")
    private String consumptionLvl;
    @ApiModelProperty(value = "家务水平")
    @TableField("homework_lvl")
    private String homeworkLvl;
    @ApiModelProperty(value = "家务分配")
    @TableField("homework_allot")
    private String homeworkAllot;
    @ApiModelProperty(value = "关于宠物")
    @TableField("pets_")
    private String pets;
    @ApiModelProperty(value = "内心独白")
    @TableField("inner_soliloquy")
    private String innerSoliloquy;


    public Long getMemberId() {
        return memberId;
    }

    public TMemberLifeway setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getHealthHabit() {
        return healthHabit;
    }

    public TMemberLifeway setHealthHabit(String healthHabit) {
        this.healthHabit = healthHabit;
        return this;
    }

    public String getSmoke() {
        return smoke;
    }

    public TMemberLifeway setSmoke(String smoke) {
        this.smoke = smoke;
        return this;
    }

    public String getDrink() {
        return drink;
    }

    public TMemberLifeway setDrink(String drink) {
        this.drink = drink;
        return this;
    }

    public String getExercise() {
        return exercise;
    }

    public TMemberLifeway setExercise(String exercise) {
        this.exercise = exercise;
        return this;
    }

    public String getDiet() {
        return diet;
    }

    public TMemberLifeway setDiet(String diet) {
        this.diet = diet;
        return this;
    }

    public String getReligion() {
        return religion;
    }

    public TMemberLifeway setReligion(String religion) {
        this.religion = religion;
        return this;
    }

    public String getWorkRest() {
        return workRest;
    }

    public TMemberLifeway setWorkRest(String workRest) {
        this.workRest = workRest;
        return this;
    }

    public String getIncomeLvl() {
        return incomeLvl;
    }

    public TMemberLifeway setIncomeLvl(String incomeLvl) {
        this.incomeLvl = incomeLvl;
        return this;
    }

    public String getConsumptionLvl() {
        return consumptionLvl;
    }

    public TMemberLifeway setConsumptionLvl(String consumptionLvl) {
        this.consumptionLvl = consumptionLvl;
        return this;
    }

    public String getHomeworkLvl() {
        return homeworkLvl;
    }

    public TMemberLifeway setHomeworkLvl(String homeworkLvl) {
        this.homeworkLvl = homeworkLvl;
        return this;
    }

    public String getHomeworkAllot() {
        return homeworkAllot;
    }

    public TMemberLifeway setHomeworkAllot(String homeworkAllot) {
        this.homeworkAllot = homeworkAllot;
        return this;
    }

    public String getPets() {
        return pets;
    }

    public TMemberLifeway setPets(String pets) {
        this.pets = pets;
        return this;
    }

    public String getInnerSoliloquy() {
        return innerSoliloquy;
    }

    public TMemberLifeway setInnerSoliloquy(String innerSoliloquy) {
        this.innerSoliloquy = innerSoliloquy;
        return this;
    }

}