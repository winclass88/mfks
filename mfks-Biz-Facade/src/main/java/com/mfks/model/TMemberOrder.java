package com.mfks.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 订单信息
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@ApiModel("订单信息")
@TableName("t_member_order")
@SuppressWarnings("serial")
public class TMemberOrder extends BaseModel {

    @ApiModelProperty(value = "会员")
    @TableField("member_id")
    private Long memberId;
    @ApiModelProperty(value = "订单号")
    @TableField("order_no")
    private String orderNo;
    @ApiModelProperty(value = "业务类型")
    @TableField("biz_type")
    private String bizType;
    @ApiModelProperty(value = "交易类型(1收入2支出)")
    @TableField("trade_type")
    private String tradeType;
    @ApiModelProperty(value = "钥匙数量")
    @TableField("key_num")
    private Integer keyNum;
    @ApiModelProperty(value = "支付方式")
    @TableField("pay_type")
    private String payType;
    @ApiModelProperty(value = "支付金额")
    @TableField("pay_money")
    private BigDecimal payMoney;
    @ApiModelProperty(value = "支付流水号")
    @TableField("pay_order_id")
    private String payOrderId;
    @ApiModelProperty(value = "支付时间")
    @TableField("pay_time")
    private Date payTime;
    @ApiModelProperty(value = "支付结果")
    @TableField("pay_result")
    private String payResult;
    @TableField("pay_open_id")
    private String payOpenId;

    @TableField(exist = false)
    private String clientIP;

    public Long getMemberId() {
        return memberId;
    }

    public TMemberOrder setMemberId(Long memberId) {
        this.memberId = memberId;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public TMemberOrder setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public String getBizType() {
        return bizType;
    }

    public TMemberOrder setBizType(String bizType) {
        this.bizType = bizType;
        return this;
    }

    public String getTradeType() {
        return tradeType;
    }

    public TMemberOrder setTradeType(String tradeType) {
        this.tradeType = tradeType;
        return this;
    }

    public Integer getKeyNum() {
        return keyNum;
    }

    public TMemberOrder setKeyNum(Integer keyNum) {
        this.keyNum = keyNum;
        return this;
    }

    public String getPayType() {
        return payType;
    }

    public TMemberOrder setPayType(String payType) {
        this.payType = payType;
        return this;
    }

    public BigDecimal getPayMoney() {
        return payMoney;
    }

    public TMemberOrder setPayMoney(BigDecimal payMoney) {
        this.payMoney = payMoney;
        return this;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public TMemberOrder setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
        return this;
    }

    public Date getPayTime() {
        return payTime;
    }

    public TMemberOrder setPayTime(Date payTime) {
        this.payTime = payTime;
        return this;
    }

    public String getPayResult() {
        return payResult;
    }

    public TMemberOrder setPayResult(String payResult) {
        this.payResult = payResult;
        return this;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public String getPayOpenId() {
        return payOpenId;
    }

    public void setPayOpenId(String payOpenId) {
        this.payOpenId = payOpenId;
    }
}