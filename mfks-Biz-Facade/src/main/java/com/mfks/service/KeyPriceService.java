package com.mfks.service;

import com.mfks.model.TKeyPrice;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 钥匙价格  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface KeyPriceService extends BaseService<TKeyPrice> {
	
}