package com.mfks.service;

import com.mfks.model.TMemberPhoto;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 会员头像  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
public interface MemberPhotoService extends BaseService<TMemberPhoto> {

}