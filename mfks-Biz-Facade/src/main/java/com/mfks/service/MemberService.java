package com.mfks.service;

import java.util.Map;

import com.mfks.bean.Member;
import com.mfks.bean.WxUserInfo;
import com.mfks.model.TMember;

import top.ibase4j.core.base.BaseService;
import top.ibase4j.core.support.Pagination;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午10:45:11
 */
public interface MemberService extends BaseService<TMember> {
    TMember updateAll(TMember record);

    Pagination<Member> queryBaseInfo(Map<String, Object> params);

    TMember getInfo(Long id);

    Object updataphone(Map<String, Object> map);

    Object authentication(Map<String, Object> map);// 实名认证

    TMember validOpenId(WxUserInfo userInfo);
}
