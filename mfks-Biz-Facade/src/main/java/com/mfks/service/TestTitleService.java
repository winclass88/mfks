package com.mfks.service;

import java.util.Map;
import java.util.Set;

import com.mfks.model.TMemberTest;
import com.mfks.model.TTestTitle;

import top.ibase4j.core.base.BaseService;
import top.ibase4j.core.support.Pagination;

/**
 * <p>
 * 测试题目  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TestTitleService extends BaseService<TTestTitle> {

    Pagination<TTestTitle> randomTestTitles(Map<String, Object> params);

    Set<TMemberTest> submit(Long memberId, String[] answers);
}