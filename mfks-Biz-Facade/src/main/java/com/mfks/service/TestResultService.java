package com.mfks.service;

import com.mfks.model.TTestResult;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 测试结果  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TestResultService extends BaseService<TTestResult> {
	
}