package com.mfks.service;

import com.mfks.model.TMemberInterest;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 会员兴趣爱好  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
public interface MemberInterestService extends BaseService<TMemberInterest> {

	TMemberInterest queryByMemberId(Long memberId);

}