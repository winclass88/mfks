package com.mfks.service;

import com.mfks.model.TTestOption;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 测试题目选项  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TestOptionService extends BaseService<TTestOption> {
	
}