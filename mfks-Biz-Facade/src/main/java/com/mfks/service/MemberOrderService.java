package com.mfks.service;

import java.util.Map;

import com.mfks.model.TMemberOrder;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 订单信息  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
public interface MemberOrderService extends BaseService<TMemberOrder> {
    Object create(TMemberOrder record);

    void updateWxPayReturn(Map<String, Object> param);

    Map<String, String> updateOrderState(TMemberOrder param);
}