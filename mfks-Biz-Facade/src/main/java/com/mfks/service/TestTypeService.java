package com.mfks.service;

import com.mfks.model.TTestType;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 测试类型  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
public interface TestTypeService extends BaseService<TTestType> {
	
}