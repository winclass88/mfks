package com.mfks.service;

import java.util.Map;

import com.mfks.bean.Member;
import com.mfks.model.TMemberFavorite;

import top.ibase4j.core.base.BaseService;
import top.ibase4j.core.support.Pagination;

/**
 * <p>
 * 收藏  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
public interface MemberFavoriteService extends BaseService<TMemberFavorite> {

    Pagination<Member> queryFavMember(Map<String, Object> params);

    Pagination<Member> queryMember(Map<String, Object> params);

}