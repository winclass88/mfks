package com.mfks.service;

import com.mfks.model.TMemberTest;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 用户测试  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2019-01-24
 */
public interface MemberTestService extends BaseService<TMemberTest> {
	
}