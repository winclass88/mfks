package com.mfks.service;

import com.mfks.model.TMemberLifeway;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 会员生活方式  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
public interface MemberLifewayService extends BaseService<TMemberLifeway> {

	TMemberLifeway queryByMemberId(Long id);

}