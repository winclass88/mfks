package com.mfks.service;

import com.mfks.model.TMemberMarriage;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 会员婚姻观念  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
public interface MemberMarriageService extends BaseService<TMemberMarriage> {

	TMemberMarriage queryByMemberId(Long id);

}