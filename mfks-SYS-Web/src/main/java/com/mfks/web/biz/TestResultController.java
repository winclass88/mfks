package com.mfks.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfks.model.TTestResult;
import com.mfks.service.TestResultService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 测试结果  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Controller
@RequestMapping("/testResult")
@Api(value = "测试结果接口", description = "测试结果接口")
public class TestResultController extends BaseController<TTestResult, TestResultService> {
    @RequiresPermissions("testResult.read")
    @GetMapping(value = "/read/list")
    @ApiOperation(value = "查询全部测试结果", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.queryList(param);
    }

    @RequiresPermissions("testResult.read")
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询测试结果", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("testResult.read")
    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "测试结果详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TTestResult param = WebUtil.getParameter(request, TTestResult.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("testResult.update")
    @ApiOperation(value = "修改测试结果", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TTestResult param = WebUtil.getParameter(request, TTestResult.class);
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("testResult.delete")
    @ApiOperation(value = "删除测试结果", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TTestResult param = WebUtil.getParameter(request, TTestResult.class);
        return super.delete(param);
    }
}