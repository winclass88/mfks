package com.mfks.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfks.model.TTestTitle;
import com.mfks.service.TestTitleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 测试题目  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Controller
@RequestMapping("/testTitle")
@Api(value = "测试题目接口", description = "测试题目接口")
public class TestTitleController extends BaseController<TTestTitle, TestTitleService> {
    @RequiresPermissions("testTitle.read")
    @GetMapping(value = "/read/list")
    @ApiOperation(value = "查询全部测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.queryList(param);
    }

    @RequiresPermissions("testTitle.read")
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("testTitle.read")
    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "测试题目详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TTestTitle param = WebUtil.getParameter(request, TTestTitle.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("testTitle.update")
    @ApiOperation(value = "修改测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TTestTitle param = WebUtil.getParameter(request, TTestTitle.class);
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("testTitle.delete")
    @ApiOperation(value = "删除测试题目", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TTestTitle param = WebUtil.getParameter(request, TTestTitle.class);
        return super.delete(param);
    }
}