package com.mfks.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfks.model.TMemberOrder;
import com.mfks.service.MemberOrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 订单信息  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-27
 */
@Controller
@RequestMapping("/memberOrder")
@Api(value = "订单信息接口", description = "订单信息接口")
public class MemberOrderController extends BaseController<TMemberOrder, MemberOrderService> {
    @RequiresPermissions("memberOrder.read")
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询订单信息", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("memberOrder.read")
    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "订单信息详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("memberOrder.update")
    @ApiOperation(value = "修改订单信息", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("memberOrder.delete")
    @ApiOperation(value = "删除订单信息", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TMemberOrder param = WebUtil.getParameter(request, TMemberOrder.class);
        return super.delete(param);
    }
}