package com.mfks.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfks.model.TTestOption;
import com.mfks.service.TestOptionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 测试题目选项  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-28
 */
@Controller
@RequestMapping("/testOption")
@Api(value = "测试题目选项接口", description = "测试题目选项接口")
public class TestOptionController extends BaseController<TTestOption, TestOptionService> {
    @RequiresPermissions("testOption.read")
    @GetMapping(value = "/read/list")
    @ApiOperation(value = "查询全部测试题目选项", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.queryList(param);
    }

    @RequiresPermissions("testOption.read")
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询测试题目选项", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("testOption.read")
    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "测试题目选项详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TTestOption param = WebUtil.getParameter(request, TTestOption.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("testOption.update")
    @ApiOperation(value = "修改测试题目选项", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TTestOption param = WebUtil.getParameter(request, TTestOption.class);
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("testOption.delete")
    @ApiOperation(value = "删除测试题目选项", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TTestOption param = WebUtil.getParameter(request, TTestOption.class);
        return super.delete(param);
    }
}