package com.mfks.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfks.model.TMember;
import com.mfks.service.MemberService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 会员  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-12-26
 */
@Controller
@RequestMapping("/member")
@Api(value = "会员接口", description = "会员接口")
public class MemberController extends BaseController<TMember, MemberService> {
    @RequiresPermissions("member.read")
    @GetMapping(value = "/read/list")
    @ApiOperation(value = "查询全部会员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.queryList(param);
    }

    @RequiresPermissions("member.read")
    @GetMapping(value = "/read/page")
    @ApiOperation(value = "分页查询会员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("member.read")
    @GetMapping(value = "/read/detail")
    @ApiOperation(value = "会员详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TMember param = WebUtil.getParameter(request, TMember.class);
        Object result = service.getInfo(param.getId());
        return setSuccessModelMap(result);
    }

    @PostMapping
    @RequiresPermissions("member.update")
    @ApiOperation(value = "修改会员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TMember param = WebUtil.getParameter(request, TMember.class);
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("member.delete")
    @ApiOperation(value = "删除会员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TMember param = WebUtil.getParameter(request, TMember.class);
        return super.delete(param);
    }
}