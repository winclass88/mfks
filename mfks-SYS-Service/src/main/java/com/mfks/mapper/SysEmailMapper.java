package com.mfks.mapper;

import com.mfks.model.SysEmail;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-01-29
 */
public interface SysEmailMapper extends BaseMapper<SysEmail> {

}