package com.mfks.mapper;

import com.mfks.model.SysNews;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-01-29
 */
public interface SysNewsMapper extends BaseMapper<SysNews> {

}