package com.mfks.mapper;

import com.mfks.model.SysUser;

import top.ibase4j.core.base.BaseMapper;

/**
 * @author ShenHuaJie
 * @since 2018年3月3日 下午7:24:50
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
