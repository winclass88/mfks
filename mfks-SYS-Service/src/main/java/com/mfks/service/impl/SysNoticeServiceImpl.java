package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.SysNoticeMapper;
import com.mfks.model.SysNotice;
import com.mfks.service.SysNoticeService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysNotice")
@Service(interfaceClass = SysNoticeService.class)
public class SysNoticeServiceImpl extends BaseServiceImpl<SysNotice, SysNoticeMapper> implements SysNoticeService {

}
