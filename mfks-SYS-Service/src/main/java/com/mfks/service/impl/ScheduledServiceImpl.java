package com.mfks.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.service.SchedulerService;

import top.ibase4j.core.support.scheduler.SchedulerServiceImpl;

/**
 * @author ShenHuaJie
 * @version 2016年7月1日 上午11:34:59
 */
@Service(interfaceClass = SchedulerService.class)
public class ScheduledServiceImpl extends SchedulerServiceImpl implements SchedulerService {

}