package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.SysMsgConfigMapper;
import com.mfks.model.SysMsgConfig;
import com.mfks.service.SysMsgConfigService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "sysMsgConfig")
@Service(interfaceClass = SysMsgConfigService.class)
public class SysMsgConfigServiceImpl extends BaseServiceImpl<SysMsgConfig, SysMsgConfigMapper>
implements SysMsgConfigService {

}
