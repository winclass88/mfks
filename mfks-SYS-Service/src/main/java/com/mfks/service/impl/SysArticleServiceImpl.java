package com.mfks.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.mfks.mapper.SysArticleMapper;
import com.mfks.model.SysArticle;
import com.mfks.service.SysArticleService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 * 文章  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "sysArticle")
@Service(interfaceClass = SysArticleService.class)
public class SysArticleServiceImpl extends BaseServiceImpl<SysArticle, SysArticleMapper> implements SysArticleService {

}
