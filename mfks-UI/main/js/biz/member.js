define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/member/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/member/read/page',
                    add_url: 'memberEdit.html',
                    edit_url: 'memberEdit.html',
                    del_url: '/member',
                    multi_url: '/member',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'avatar', title: '头像', formatter: function (v, row, index) {
                        	return v ? '<img class="view" title="' + row.userName + '" height="30px" src="'+v+'">' : '';
                        }},
                        {field: 'nickName', title: '昵称'},
                        {field: 'phone', title: __('Mobile')},
                        {field: 'userName', title: __('Username')},
                        {field: 'sex', title: '性别', formatter: function (v, row, index) {
                        	return v == '1' ? '男' : v == '2' ? '女' : '保密';
                        }},
                        {field: 'age', title: '年龄'},
                        {field: 'stature', title: '身高'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.userType == 3) {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        	Util.api.initSelectOption({
            	target: '[name="locationProvince"]',
            	param: {type: 'PROVINCE'},
            	init: function() {
            		var id = Util.api.query('id');
                	var t = Util.api.query('t');
                	if(id && t === 'U') {
                		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
            				if(data.avatar) {
            					$('#avatar').append('<img height="30px" src="'+data.avatar+'">');
            				}
            				if($('[name="sex"][value="' + data.sex + '"]').length > 0) {
                    			$('[name="sex"][value="' + data.sex + '"]').get(0).checked = true;
                    			$('[name="sex"][value="' + data.sex + '"]').parents('span').addClass('checked');
                			}
                			$('[name="enable"]').bootstrapSwitch('state', data.enable == '1');
                		});
                	}
            	}
            });
        }
    };
    return Controller;
});