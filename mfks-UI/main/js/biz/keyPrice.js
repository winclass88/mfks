define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/keyPrice/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/keyPrice/read/page',
                    add_url: 'keyPriceEdit.html',
                    edit_url: 'keyPriceEdit.html',
                    del_url: '/keyPrice',
                    multi_url: '/keyPrice',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'keyNum', title: '钥匙数量'},
                        {field: 'payMoney', title: '价格'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
        		});
        	}
        }
    };
    return Controller;
});