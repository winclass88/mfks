define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/memberOrder/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/memberOrder/read/page',
                    del_url: '/memberOrder',
                    multi_url: '/memberOrder',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'orderNo', title: '订单号'},
                        {field: 'bizType', title: '业务类型'},
                        {field: 'tradeType', title: '交易类型', formatter: function (v, row, index) {
                        	return v == '1' ? '收入' : v == '2' ? '支出' : '--';
                        }},
                        {field: 'keyNum', title: '钥匙数'},
                        {field: 'payMoney', title: '支付金额'},
                        {field: 'payOrderId', title: '支付流水'},
                        {field: 'payTime', title: '支付时间'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
        		});
        	}
        }
    };
    return Controller;
});