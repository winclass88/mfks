define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/testType/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/testType/read/page',
                    add_url: 'testTypeEdit.html',
                    edit_url: 'testTypeEdit.html',
                    del_url: '/testType',
                    multi_url: '/testType',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'testType', title: '测试类型'},
                        {field: 'imgUrl', title: '配图', formatter: function (v, row, index) {
                        	return v ? '<img class="view" title="' + row.testType + '" height="30px" src="'+v+'">' : '';
                        }},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
    				if(data.imgUrl) {
    					$('#imgUrl').append('<img height="30px" src="'+data.imgUrl+'">');
    				}
        		});
        	}
        }
    };
    return Controller;
});