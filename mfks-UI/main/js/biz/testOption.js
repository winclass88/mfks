define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/testOption/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/testOption/read/page?testId=' + Util.api.query('id'),
                    add_url: 'testOptionEdit.html',
                    edit_url: 'testOptionEdit.html',
                    del_url: '/testOption',
                    multi_url: '/testOption',
                }
            });

            $('#testId').val(Util.api.query('id'));
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'testOption', title: '选项', formatter: function (v, row, index) {
                        	return row.optionKey ? row.optionKey + '、' + v : v;
                        }},
                        {field: 'optionVal', title: '选项分值'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            $('#testId').val($('#testId', window.parent.document).val());
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
    				if(data.imgUrl) {
    					$('#imgUrl').append('<img height="30px" src="'+data.imgUrl+'">');
    				}
        		});
        	}
        }
    };
    return Controller;
});