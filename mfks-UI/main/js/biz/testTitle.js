define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/testTitle/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/testTitle/read/page',
                    add_url: 'testTitleEdit.html',
                    edit_url: 'testTitleEdit.html',
                    del_url: '/testTitle',
                    multi_url: '/testTitle',
                }
            });

            var table = $("#table");
            var testTypes = ['','IQ测评','性格评测','EQ评测','心理评测','心智评测','观念评测'];
            var titleTypes = ['', '单选题', '多选题', '填空题', '排序题'];
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'testType', title: '类型', formatter: function (v, row, index) {
                        	return testTypes[v] + '(' + titleTypes[row.titleType] + ')';
                        }},
                        {field: 'testTitle', title: '题目', align:'left', formatter: function (v, row, index) {
                        	var i = row.imgUrl ? '<img class="view" title="' + v + '" height="30px" src="'+row.imgUrl+'">' : '';
                        	v = v.length > 40 ? v.substr(0, 40) + '... ...' : v;
                        	return i + (row.titleSeq ? row.titleSeq + '、' + v : v);
                        }},
                        {field: 'titleVal', title: '分值'},
                        {field: 'operate', title: __('Operate'), events: $.extend({
           		       	 'click .btn-options': function (e, value, row, index) {
        		             e.stopPropagation();
        		             var options = $(this).closest('table').bootstrapTable('getOptions');
                             var url = 'testOption.html' + "?id="+ row[options.pk];
                             Util.api.open(url, '测评选项-' + row.testTitle);
        		       	 }
        		    	},Table.api.events.operate), formatter: function (value, row, index) {
                                return '<a href="javascript:;" class="btn btn-primary btn-options btn-xs"><i class="fa fa-th-large"></i></a> '
                                +Table.api.formatter.operate.call(this, value, row, index, table);
                         }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
    				if(data.imgUrl) {
    					$('#imgUrl').parent().append('<img height="30px" src="'+data.imgUrl+'">');
    				}
        		});
        	}
        }
    };
    return Controller;
});