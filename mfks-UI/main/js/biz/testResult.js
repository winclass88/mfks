define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/testResult/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/testResult/read/page',
                    add_url: 'testResultEdit.html',
                    edit_url: 'testResultEdit.html',
                    del_url: '/testResult',
                    multi_url: '/testResult',
                }
            });

            var table = $("#table");
            var testTypes = ['','IQ测评','性格评测','EQ评测','心理评测','心智评测','观念评测'];
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'testType', title: '类型', formatter: function (v, r, index) {
                        	return r.testGroup ? testTypes[v] + "." + r.testGroup : testTypes[v];
                        }},
                        {field: 'valMin', title: '分值', formatter: function(v, r, i) {
                        	return v + "~" + r.valMax;
                        }},
                        {field: 'resultTitle', title: '结果'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                            return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
    				if(data.imgUrl) {
    					$('#imgUrl').append('<img height="30px" src="'+data.imgUrl+'">');
    				}
        		});
        	}
        }
    };
    return Controller;
});