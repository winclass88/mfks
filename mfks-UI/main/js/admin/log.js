define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    	},
        init: function () {
            $('[href="#eventLog"]').on('click', function() {
	            // 初始化表格参数配置
	            Table.api.init({
	            	singleSelect: true,
	                extend: {
	                    index_url: '/event/read/page',
	                }
	            });
            });
            $('[href="#eventLog"]').click();
            var eventLogTable = $("#eventLogTable");
            // 初始化表格
            eventLogTable.bootstrapTable({
            	toolbar: '#toolbar1',
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                  	    {field: 'title', title: '操作'},
                        {field: 'userName', title: '操作人', formatter: function (value, row, index) {
                        	return value && row.userPhone ? value + '(' + row.userPhone + ')' : value ? value : row.userPhone;
                        }},
                        {field: 'createTime', title: '操作时间'},
                        {field: 'clientHost', title: 'IP'},
                        {field: 'userAgent', title: '浏览器'},
                        {field: 'remark', title: '备注'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                            return Table.api.formatter.operate.call(this, value, row, index, eventLogTable);
                        }}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(eventLogTable);
            
            var i = 0;
            $('[href="#taskLog"]').on('click', function() {
            	// 初始化表格参数配置
                Table.api.init({
                	singleSelect: true,
                    extend: {
                        index_url: '/scheduled/read/log',
                    }
                });
            	if(i === 0) {
                    var taskLogTable = $("#taskLogTable");
                    // 初始化表格
                    taskLogTable.bootstrapTable({
                    	toolbar: '#toolbar2',
                        url: $.fn.bootstrapTable.defaults.extend.index_url,
                        columns: [
                            [
                                {field: 'id', title: 'ID'},
                          	    {field: 'groupName', title: '任务组'},
                                {field: 'taskName', title: '任务名称'},
                                {field: 'startTime', title: '开始时间'},
                                {field: 'endTime', title: '结束时间'},
                                {field: 'serverHost', title: '服务器'},
                                {field: 'status', title: '状态', formatter: function(data, type, row) {
                         		   return data == 'S' ? '<span class="label label-info">成功</span>' : '<span class="label label-warning">失败</span>';
                                }},
                                {field: 'fireInfo', title: '备注'},
                                {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                    return Table.api.formatter.operate.call(this, value, row, index, taskLogTable);
                                }}
                            ]
                        ]
                    });
                    // 为表格绑定事件
                    Table.api.bindevent(taskLogTable);
                    i++;
            	}
            });
        }
    };
    return Controller;
});