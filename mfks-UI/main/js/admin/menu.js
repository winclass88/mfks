define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'treetable', 'ztree'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/menu/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	pagination: false,
            	singleSelect: true,
                extend: {
                    index_url: '/menu/read/list',
                    add_url: 'menuEdit.html',
                    edit_url: 'menuEdit.html',
                    del_url: '/menu',
                },
                onPostBody: function() {
					$.each($('tr').find('td:eq(0)'), function(i, td) {
						var s = $(td).find('span');
						s.parents('tr').attr('data-tt-id', s.attr('data-t-id')).attr('data-tt-name', s.attr('data-t-name'));
						s.parents('tr').attr('data-tt-parent-id', s.attr('data-t-parent-id')).attr('data-tt-parent-name', s.attr('data-t-parent-name'));
					});
					// 表格树
					table.treetable('destroy').treetable({
					    expandable: false
					});
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'menuName', title: __('Menu'), align:'left', formatter: function (value, row, index) {
                        	return row.leaf == 0 ? '<span class="folder" data-t-id="'+row.id+'" data-t-name="'+row.menuName
                        			+'" data-t-parent-id="'+row.parentId+'" data-t-parent-name="'+row.parentName+'">'+value+'</span>'
                        			: '<span class="file" data-t-id="'+row.id+'" data-t-name="'+row.menuName
                        			+'" data-t-parent-id="'+row.parentId+'" data-t-parent-name="'+row.parentName+'">'+value+'</span>';
                        } },
                    	{field:'state', checkbox: true, 'class': 'hide'},
                        {field: 'iconcls', title: __('Icon'), formatter: function(value, row, index) {
                        	return '<i class="' + value + '"></i>'
                        }},
                        {field: 'request', title: 'URL'},
                        {field: 'isShow', title: __('Status'), formatter: function (value, row, index, custom) {
                        	if(value === '1' || value === 'true') {
        	               		return '<span class="label label-info">显示</span>';
        	               	} else  {
        	               		return '<span class="label label-default">隐藏</span>';
        	               	}
                        }},
                        {field: 'sortNo', title: __('Weigh')},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.menuType == '1') {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        	//本地验证未通过时提示
        	$("form[role=form]").data("validator-options", {
                invalid: function (form, errors) {
                    $.each(errors, function (i, j) {
                        Toastr.error(j);
                    });
                },
                target: '#errtips'
            });
        	var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id) {
        		Util.api.initSelectTree({
        			checkParent: true,
        			parentEle: 'menuParent',
        			url: '/menu/read/list',
        			data: {},
        			resetData: function(rows) {
        				$.each(rows, function(i, d) {
        					d['name'] = d.menuName;
        					d['pId'] = d.parentId;
        				});
        				rows.unshift({id: '0', name: '顶级菜单', open: true});
        			}
        		});
    			if(t == 'A') {
            		Util.api.ajax({ 
            			type: 'get',
            			url: Controller.config.get_url,
            			data: {id: id}
            		}, function(data) {
                    	$('[name="parentId"]').val(data.id);
                    	$('[name="parentName"]').val(data.menuName);
            		});
    			} else if(t== 'U') {
    				Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
    					if(data.parentId === '0') {
    						$('[name="parentName"]').val('顶级菜单');
    					}
            			$('[name="isShow"]').bootstrapSwitch('state', data.isShow === '1');
            		});
    			}
        	} else {
        		$('[name="parentId"]').val('0');
        		$('[name="parentName"]').val('顶级菜单');
        	}
        }
    };
    return Controller;
});