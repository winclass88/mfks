define(['jquery', 'bootstrap', 'utilback', 'form', 'ztree'], function ($, undefined, Utilback, Form) {

    var Controller = {
    	config: {
    		get_url: '/department/read/detail',
    	},
        init: function () {
        	$('#edit-form button').hide();
        	$('.hide').removeClass('hide');
            //新增职位
            function addHoverDom(treeId, treeNode) {
                var sObj = $("#" + treeNode.tId + "_span"); //获取节点信息
                if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;

                var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' data-id='"
                	+ treeNode.id + "' data-name='" + treeNode.deptName
                	+ "' title='新增' onfocus='this.blur();'></span>"; //定义添加按钮
                sObj.after(addStr); //加载添加按钮
                var btn = $("#addBtn_" + treeNode.tId);

                //添加部门
                if (btn) btn.bind("click", function (event) {
                    var zTree = $.fn.zTree.getZTreeObj("tree");
                    $('#edit-form input,#edit-form select,#edit-form textarea').removeAttr('disabled').val('');
                    $('#edit-form input[name="parentId"]').val($(this).attr('data-id'));
                    $('#edit-form input[name="parentName"]').val($(this).attr('data-name'));
                	$('#edit-form button').show();
                    event.stopPropagation();
                });
            };

            function removeHoverDom(treeId, treeNode) {
            	$("#addBtn_" + treeNode.tId).remove();
            };

            //点击部门显示对应信息
            function zTreeOnClick(event, treeId, treeNode) {
            	$('#edit-form button').hide();
                $('#edit-form').autofill(treeNode);
                $('#edit-form input,#edit-form select,#edit-form textarea').attr("disabled", 'disabled');
            }

            //编辑部门信息
            function editDom(treeId, treeNode) {
            	$('#edit-form button').show();
                $('#edit-form input,#edit-form select,#edit-form textarea').removeAttr('disabled');
                $('#edit-form').autofill(treeNode);
                return false;
            }
            function deleteDom(treeId, treeNode) {
            	Util.api.ajax({
             	   type: 'DELETE',
             	   data: {id: treeNode.id},
             	   url: '/dept',
                 });
            	return false;
            }
        	/**
             * 部门树结构
             */
            var setting = {
                view: {
                    addHoverDom: addHoverDom, //当鼠标移动到节点上时，显示用户自定义控件
                    removeHoverDom: removeHoverDom//离开节点时的操作
                },
                check: {
                    enable: false,
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                edit: {
                    enable: true
                },
                treeNode: {
                    checked: false
                },
                callback: {
                    onClick: zTreeOnClick, //单击事件
                    beforeEditName: editDom, //编辑
                    beforeRemove: deleteDom
                }
            };
            function initDeptTree() {
                Util.api.ajax({
            	   type: 'get',
            	   data: {keyword:''},
            	   url: '/dept/read/list',
                }, function(result) {
        			   var data = result.rows;
        			   for(var i=0;i<data.length;i++) {
        				   if(data[i].leaf == 0) {
         					   data[i]['open'] = true;
        				   }
        				   data[i]['pId'] = data[i].parentId;
        				   data[i]['name'] = data[i].deptName;
        			   }
        		       $.fn.zTree.init($("#departmentTree"), setting, data);
        		       
    				   Util.api.initSelectTree({
    		            	checkParent: true,
    		            	parentEle: 'deptEdit',
    		        		data: data
    		        	});
             	});
            }
		    initDeptTree();
		    $('#toolbar').on('click', '.btn-refresh', function() {
		    	initDeptTree();
		    });
            Form.api.bindevent($("form[role=form]"), function(){
            	return true;
            }, function() {
            	initDeptTree();
            });
        }
    };
    return Controller;
});