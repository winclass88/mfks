define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'ztree', 'md5'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/user/read/detail', // 获取用户详情
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/user/read/page', // 查询列表
                    add_url: 'userEdit.html', // 新增页面
                    edit_url: 'userEdit.html', // 修改页面
                    del_url: '/user', // 删除
                    multi_url: '/user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'userName', title: __('Username')},
                        {field: 'account', title: __('Account')},
                        {field: 'phone', title: __('Mobile')},
                        {field: 'deptName', title: __('Deptname')},
                        {field: 'email', title: __('Email')},
                        {field: 'enable', title: __("Status"), formatter: Table.api.formatter.enable},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
	                        	if(row.userType != 1) {
	                                return '';
	                            }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () { // 初始化编辑页面
            Form.api.bindevent($("form[role=form]"));
        	Util.api.initSelectTree({
            	checkParent: true,
            	parentEle: 'department',
        		url: '/dept/read/list',
        		data: {keyword:''},
        		resetData: function(data) {
        		   for(var i=0;i<data.length;i++) {
        			   data[i]['open'] = true;
        			   data[i]['pId'] = data[i].parentId;
        			   data[i]['name'] = data[i].deptName;
        		   }
        		   initForm();
        		   return data;
            	}
            });
        	function initForm() {
	        	var id = Util.api.query('id');
	        	var t = Util.api.query('t');
	        	if(id && t === 'U') {
	        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
	        			$('[name="enable"]').bootstrapSwitch('state', data.enable == '1');
	        		});
	        	}
        	}
        },
        update: function() {
            Form.api.bindevent($("form[role=form]"));
        	Util.api.ajax({
        		type: 'GET',
        		url: '/user/read/current'
        	}, function(data) {
        		data.birthDay && (data.birthDay = data.birthDay.substr(0, 10));
        		$('#edit-form').autofill(data);
        	});
        }
    };
    return Controller;
});