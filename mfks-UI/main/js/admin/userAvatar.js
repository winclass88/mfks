define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'sitelogo', 'cropper'], function ($, undefined, Utilback, Table, Form) {
    var Controller = {
    	config: {
    		
    	},
        init: function () {
        	Util.api.ajax({
        		type:'GET',
    			url : '/user/read/current'
        	}, function(result) {
				if (result.avatar) {
					$('img.avatar').attr('src', result.avatar);
				}
    		});
        	
        	window.submitDone = function(result) {
    			if (result.data.avatar) {
    				$('img.avatar').attr('src', result.data.avatar);
    				$('img.avatar',window.parent.document).attr('src', result.data.avatar);
    				 parent.Toastr.success("保存成功");
                    var index = parent.Layer.getFrameIndex(window.name);
                    parent.Layer.close(index);
    			}
    		}
        	
        	$('#edit-form').on('submit', function() {
        		return false;
        	});
        }
    };
    return Controller;
});