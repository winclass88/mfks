define(['jquery', 'bootstrap', 'utilback', 'form', 'md5'], function ($, undefined, UtilBack, Form) {
    var Controller = {
    		init: function () {
                var tmp = Util.api.get('skin');
                if (tmp && $.inArray(tmp, my_skins) > -1)
                    Util.api.change_skin(tmp);
                var lastlogin = Util.api.get("lastlogin");
                if (lastlogin) {
                    lastlogin = JSON.parse(lastlogin);
                    $("#profile-img").attr("src", UtilBack.api.cdnurl(lastlogin.avatar));
                    $("#pd-form-username").val(lastlogin.account);
                }

                //本地验证未通过时提示
                $("#login-form").data("validator-options", {
                    invalid: function (form, errors) {
                        $.each(errors, function (i, j) {
                            Toastr.error(j);
                        });
                    },
                    target: '#errtips'
                });

                //为表单绑定事件
               Form.api.bindevent($("#login-form"), null, function (data) {
        			Util.api.store("lastlogin", JSON.stringify({ account: data.account, avatar: data.avatar}));
                    location.href = UtilBack.api.fixurl('/index.html');
                });
            }
    }
    return Controller;
});