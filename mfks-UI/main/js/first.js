define([ 'jquery', 'bootstrap', 'utilback', 'addtabs', 'table', 'echarts',
		'echarts-theme' ], function($, undefined, UtilBack, Datatable, Table,
		Echarts) {

	var Controller = {
		init : function() {
			// 基于准备好的dom，初始化echarts实例
			var myChart = Echarts.init(document.getElementById('mainChart'),'walden');
			var chart1 = Echarts.init(document.getElementById('chart1'),
					'macarons');
			var chart2 = Echarts.init(document.getElementById('chart2'),
					'macarons');

			var period = "D";
			$('#toolbar').on('click', '.btn:not(.disabled)', function() {
				period = $(this).attr('data-id');
				$(this).siblings().removeClass('disabled');
				$(this).addClass('disabled');
				main();
			});
			// 会员充值的时间插件
			$('#rechartToolbar').on('click', '.btn:not(.disabled)', function() {
				period = $(this).attr('data-id');
				$(this).siblings().removeClass('disabled');
				$(this).addClass('disabled');
				reChartMain();
			});
			// 单车使用统计
			$('#useToolbar').on('click','.btn:not(.disabled)',function(){
				period = $(this).attr('data-id');
				$(this).siblings().removeClass('disabled');
				$(this).addClass('disabled');
				userChart();
			})
			var d = {
				D: {
					d:["1号", "2号", "3号", "4号", "5号", "6号", "7号", "8号", "9号", "10号", "11号", "12号", "13号", "14号", "15号", "16号", "17号", "18号", "19号", "20号", "21号", "22号", "23号", "24号", "25号", "26号", "27号", "28号", "29号", "30号", "31号"],
					u: [0, 0, 100, 220, 290, 190, 100, 80, 100, 110, 133, 150, 100, 80, 111, 130, 160, 144, 110, 90, 80, 100, 110, 120, 140, 190, 270, 200, 160, 100, 0, 0],
					o: [0, 0, 80, 100, 150, 160, 170, 200, 220, 240, 200, 180, 120, 100, 160, 180, 200, 210, 250, 200, 180, 170, 200, 240, 280, 300, 330, 260, 190, 150, 0, 0],
					m: [0, 0, 960.05, 3520, 1680, 1880, 1880, 5280, 5104, 5848, 6808, 7504, 7000, 7160, 3656,2440.02,3900.01,3680.00,3470.00,3607.00,4400.00,1289.04,2696.76,4239.52,2349.00,4264.28,2958.00,4264.00,4239, 2696, 1656]
				}
			};
			function main() {
					var subtext = "累计会员：298 | 累计订单：897 | 累计交易额：1890894";
					$('#subTitle').html(subtext);
					var result = d[period];
					// 指定图表的配置项和数据
					var option = {
						backgroundColor : "#fff",
						tooltip : {
							trigger : "axis"
						},
						legend : {
							data : [ "新增会员", "新增订单", "新增订单金额" ],
							textStyle : {
								color : "#878787"
							}
						},
						grid : {
							top : 50,
							bottom : 30,
							left : 50,
							right : 40
						},
						calculable : true,
						xAxis : [ {
							type : "category",
							boundaryGap : false,
							data : result.d
						} ],
						yAxis : [ {
							type : "value"
						} ],
						series : [ {
							name : "新增会员",
							type : "line",
							smooth : true,
							itemStyle : {
								normal : {
									color : "#7b93e0",
									areaStyle : {
										type : 'default'
									}
								}
							},
							data : result.u
						}, {
							name : "新增订单",
							type : "line",
							smooth : true,
							itemStyle : {
								normal : {
									color : "#32a487",
									areaStyle : {
										type : 'default'
									}
								}
							},
							data : result.o
						}, {
							name : "新增订单金额",
							type : "line",
							smooth : true,
							itemStyle : {
								normal : {
									areaStyle : {
										type : 'default'
									}
								}
							},
							data : result.m
						} ]
					};
					// 使用刚指定的配置项和数据显示图表。
					myChart.setOption(option);
			}
			function refresh() {
				main();
				
				var a = [{name:'支付宝',value : 23}, {name:'微信',value : 22}], b = [ '支付宝金额', '微信金额', '支付宝', '微信' ];
				var am = [{name:'支付宝金额',value : 230}, {name:'微信金额',value : 233}];
				chart1.setOption({
					title : {
						text : '付款方式',
						x : 'left'
					},
					tooltip : {
						trigger : 'item',
						formatter : "{a} <br/>{b} : {c} ({d}%)"
					},
					legend : {
						x : 'right',
						orient : 'vertical',
						data : b
					},
					calculable : true,
					series : [ {
						name : '付款方式',
						type : 'pie',
						radius : '50%',
						center : [ '35%', '60%' ],
						itemStyle : {
							normal : {
								label : {
									position : 'inner',
									formatter : "{b}\n{d}%"
								},
								labelLine : {
									show : false
								}
							}
						},
						data : a
					},

					{
						name : '付款方式',
						type : 'pie',
						radius : [ '60%', '70%' ],
						center : [ '35%', '60%' ],
						itemStyle : {
							normal : {
								label : {
									show : false
								},
								labelLine : {
									show : false
								}
							}
						},
						data : am
					} ]
				});

				
				var a = [ { name :  '已付款', value : 700 }, { name :  '未付款', value : 100 }], b = [ '未付款', '已付款' ];
				chart2.setOption({
					title : {
						text : '订单状态',
						x : 'left'
					},
					tooltip : {
						trigger : 'item',
						x : 'right',
						formatter : "{a} <br/>{b} : {c} ({d}%)"
					},
					legend : {
						x : 'right',
						orient : 'vertical',
						data : b
					},
					calculable : true,
					series : [ {
						name : '单车状态',
						type : 'pie',
						radius : '70%',
						center : [ '40%', '55%' ],
						itemStyle : {
							normal : {
								label : {
									position : 'inner',
									formatter : "{b}\n{d}%"
								},
								labelLine : {
									show : false
								}
							}
						},
						data : a
					} ]
				});
			}

			refresh();
			window.setInterval(refresh, 30000);

			$(window).resize(function(t) {
				myChart.resize(t);
				chart1.resize(t);
				chart2.resize(t);
				reChart.resize(t);
				useChart.resize(t);
			});
		}
	};

	return Controller;
});